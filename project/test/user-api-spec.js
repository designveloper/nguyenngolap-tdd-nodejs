require('co-mocha');
var data = require('../user-data');
var should = require('should');
var fs = require('co-fs');

var api = require('../user-web.js');
// Also start an instance of Web API when supertest runs
var request = require('co-supertest').agent(api.listen());

before(function* () {
    yield fs.writeFile('./users.json', "[]");
});

// Data layer
describe('user data', function() {
    it('should have +1 user count after saving', function* () {
        var users = yield data.users.get();

        yield data.users.save({ name: 'John' });

        var newUsers = yield data.users.get();

        newUsers.length.should.equal(users.length + 1);
    });
});

// Web layer
describe('user web', function() {
    it('should have +1 user count after saving', function* () {
        // Supertest needs an instance of Koa passed into it
        var res = yield request.get('/user').expect(200).end();

        // var users = res.body;

        // yield data.users.save({ name: 'John' });

        // var newRes = yield request.get('/user').expect(200).end();
        // var newUsers = newRes.body;

        // newUsers.length.should.equal(users.length + 1);
    });
});