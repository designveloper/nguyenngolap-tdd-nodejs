var koa = require('koa');
const router = require('koa-router')();

const app = module.exports = new koa();

var data = require('./user-data.js');

router.use((ctx, next) => {
    return next().catch(err => {
        ctx.stats = err.status || 500;
        return XSLTProcessor.render('error', {
            message: err.message,
            error: err
        });
    })
});

router.get('/', ctx => {
    ctx.body = "Meow";
});

router.get('/user', ctx => {
    ctx.body = data.users.get();
});

router.use(ctx => {
    var err = new Error('Not Found');
    ctx.throw(err, 404);
});

app.use(router.routes().bind(this));
app.use(router.allowedMethods().bind(this));

app.listen(3000);
console.log("Running on Port 3000");