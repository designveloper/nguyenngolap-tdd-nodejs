# TDD NodeJS
Intern: Nguyen Ngo Lap

\- Mocha doesn't know how to handle generator function => Use the generator version of Mocha - `co-mocha`

\- `should` - assertion library

- https://www.npmjs.com/package/should

\- `co-fs` - file system access with generators

\- `supertest` - test the web API (web layer). To work with generators, use the `co` version

- Supertest needs an instance of Koa passed into it

\- Beware of using Koa v2 with generator functions:

- For Koa v2, use async/await (Node v7 and --harmony-async-await flag?)
- https://stackoverflow.com/questions/40392605/koa-router-not-working-with-generator